

#ifndef RESSOURCE_HPP
#define RESSOURCE_HPP

#include <memory>
#include <vector>
#include <iostream>

class Ressource {
private:
	int m_stock;

public:
	Ressource(int stock);
	int getStock() const;
	void consommer(int consommation);

};

using ressources_t = std::vector<std::weak_ptr<Ressource>>;

std::ostream& operator <<(std::ostream& os, ressources_t& r);
#endif
