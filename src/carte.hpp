
#ifndef CARTES_HPP
#define CARTES_HPP

#include "usine.hpp"

class UsineCarte;

class Carte {
	friend UsineCarte;
private:
	unsigned m_value;
	static int m_compteur;
	Carte(unsigned value);
	Carte(const Carte& carte);

public:
	~Carte();
	unsigned getValeur() const;
	static int getCompteur();
	Carte& operator=(const Carte&) = delete;
};

#endif