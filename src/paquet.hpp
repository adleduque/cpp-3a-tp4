
#ifndef PAQUET_HPP
#define PAQUET_HPP

#include <vector>
#include <memory>
#include <iostream>
#include "carte.hpp"
#include "usine.hpp"

using paquet_t = std::vector<std::unique_ptr<Carte>>;


void remplir(paquet_t & p, UsineCarte & u) {
	p.reserve(u.getNbCarte() - u.getCompteur());

	while (u.getCompteur() < u.getNbCarte()) {
		p.push_back(std::unique_ptr<Carte>( u.getCarte() ));
	}
}

std::ostream& operator <<(std::ostream& os, const paquet_t & t) {
	for(auto it = t.begin(); it != t.end(); ++it) {
		os << (*it)->getValeur() << " ";
	}
	return os;
}

#endif
