
#include "carte.hpp"
#include "usine.hpp"
#include <iostream>

UsineCarte::UsineCarte() : UsineCarte(52) {}

UsineCarte::UsineCarte(unsigned nbCarte) : m_compteur(0), m_nbCarte(nbCarte) {}

Carte* UsineCarte::getCarte() {
	if (m_compteur < m_nbCarte) {
		return new Carte(m_compteur++);
	}

	else
		return nullptr;
}

