
#include "carte.hpp"
#include <iostream>

int Carte::m_compteur = 0;

Carte::Carte(unsigned value) : m_value(value) {++m_compteur;}
Carte::Carte(const Carte& carte) : m_value(carte.m_value) {}
Carte::~Carte() {--m_compteur;}

unsigned Carte::getValeur() const {
	return m_value;
}

int Carte::getCompteur() {
	return m_compteur;
}
