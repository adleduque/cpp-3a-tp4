
#ifndef USINE_HPP
#define USINE_HPP

#include "carte.hpp"

class Carte;

class UsineCarte {
private:
	unsigned m_compteur;
	unsigned m_nbCarte;
public:
	UsineCarte();
	UsineCarte(unsigned nbCarte);
	unsigned getCompteur() const {return m_compteur;}
	unsigned getNbCarte() const {return m_nbCarte;}
	Carte* getCarte();
	UsineCarte(const UsineCarte&) = delete;
	UsineCarte& operator=(const UsineCarte& ) = delete;
};

#endif