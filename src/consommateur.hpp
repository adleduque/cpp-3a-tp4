
#ifndef CONSOMMATEUR_HPP
#define CONSOMMATEUR_HPP

#include <memory>
#include "ressource.hpp"

class Consommateur {
private:
	std::shared_ptr<Ressource> m_r;
	int m_besoin;

public:
	Consommateur(int besoin, std::shared_ptr<Ressource>& r);
	void puiser();

};

#endif