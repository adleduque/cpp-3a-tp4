
#include "consommateur.hpp"
#include "ressource.hpp"
#include <memory>

Consommateur::Consommateur(int besoin, std::shared_ptr<Ressource>& r) :
	m_r(r), m_besoin(besoin)
{}

void Consommateur::puiser() {
	if (m_r != nullptr) {
		m_r->consommer(m_besoin);
		if (m_r->getStock() == 0)
			m_r = nullptr;
	}
}

