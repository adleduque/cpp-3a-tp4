
#include "ressource.hpp"

Ressource::Ressource(int stock) : m_stock(stock) {}

int Ressource::getStock() const {
	return m_stock;
}

void Ressource::consommer(int consommation) {
	m_stock -= consommation;
	if (m_stock < 0)
		m_stock = 0;
}


std::ostream& operator <<(std::ostream& os, ressources_t& r) {
	for (auto it=r.begin(); it != r.end(); ++it) {
		if ((*it).expired()) {
			os << "- ";
		} else {
			os << (*it).lock()->getStock() << " ";
		}
	}
	return os;
}

